#!/bin/bash
wget -O apple.txt https://raw.githubusercontent.com/Loyalsoldier/clash-rules/release/apple.txt
wget -O applications.txt https://raw.githubusercontent.com/Loyalsoldier/clash-rules/release/applications.txt
wget -O cncidr.txt https://raw.githubusercontent.com/Loyalsoldier/clash-rules/release/cncidr.txt
wget -O direct.txt https://raw.githubusercontent.com/Loyalsoldier/clash-rules/release/direct.txt
wget -O gfw.txt https://raw.githubusercontent.com/Loyalsoldier/clash-rules/release/gfw.txt
# 这一条是国内可直连的google地址，不使用！！！
#wget -O google.txt https://raw.githubusercontent.com/Loyalsoldier/clash-rules/release/google.txt
wget -O icloud.txt https://raw.githubusercontent.com/Loyalsoldier/clash-rules/release/icloud.txt
wget -O lancidr.txt https://raw.githubusercontent.com/Loyalsoldier/clash-rules/release/lancidr.txt
wget -O private.txt https://raw.githubusercontent.com/Loyalsoldier/clash-rules/release/private.txt
wget -O proxy.txt https://raw.githubusercontent.com/Loyalsoldier/clash-rules/release/proxy.txt
wget -O reject.txt https://raw.githubusercontent.com/Loyalsoldier/clash-rules/release/reject.txt
wget -O telegramcidr.txt https://raw.githubusercontent.com/Loyalsoldier/clash-rules/release/telegramcidr.txt
wget -O tld-not-cn.txt https://raw.githubusercontent.com/Loyalsoldier/clash-rules/release/tld-not-cn.txt